#pragma once
#include <GLFW\glfw3.h>

#include "auxMath.h"
#include "Entity.h"

using auxMath::Vector3;
using auxMath::Matrix4x4;

class Camera : public Entity
{
public:
	Camera();
	~Camera();

	void moveTo(Vector3 shift) override;
	void moveBy(Vector3 shift) override;

	void rotateTo(float angle, Vector3 axis) override;
	void rotateBy(float angle, Vector3 axis) override;

	void onKeyPress(int p_key, int p_scancode, int p_mods);
	void onKeyRelease(int p_key, int p_scancode, int p_mods);

	void onMouseMove(double xpos, double ypos);

	void update(float dt);

	Matrix4x4 viewMatrix() const; 

private:
	float m_hAngle;
	float m_vAngle;

	bool m_forward;
	bool m_backward;
	bool m_left;
	bool m_right;
	bool m_up;
	bool m_down;
};


