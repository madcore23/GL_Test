#version 400

uniform mat4 mMatrix;
uniform mat4 mvMatrix;
uniform mat4 mvpMatrix;
uniform vec3 v_lightPos;

in vec3 position;
in vec3 v_normal;
in vec2 v_texCoord;

out vec3 f_viewVec;
out vec3 f_normal;
out vec3 f_lightPos;
out vec2 f_texCoord;

void main()
{
	f_viewVec = - (mvMatrix * vec4(position, 1.0)).xyz;
	f_normal = (mMatrix * vec4(v_normal, 0.0)).xyz;
	f_lightPos = v_lightPos + f_viewVec;
	f_texCoord = v_texCoord;
	
	gl_Position = mvpMatrix * vec4(position, 1.0);
}