#pragma once
#include "auxMath.h"

using auxMath::Vector3;
using auxMath::Matrix4x4;

class Entity
{
public:
	Entity();
	~Entity();

	virtual void moveTo(Vector3 shift);// { m_position = shift; }
	virtual void moveBy(Vector3 shift);// { m_position += shift; }

	virtual void rotateTo(float angle, Vector3 axis);// { m_rotAngle = angle; m_axis = axis; }
	virtual void rotateBy(float angle, Vector3 axis);

	virtual void onKeyPress(int p_key, int p_scancode, int p_mods) = 0;
	virtual void onKeyRelease(int p_key, int p_scancode, int p_mods) = 0;

	virtual void onMouseMove(double xpos, double ypos) = 0;

	virtual void update(float dt) = 0;

protected:
	Vector3 m_position;
	Vector3 m_axis;
	float m_rotAngle;

	Matrix4x4 m_modelMatrix;
};

