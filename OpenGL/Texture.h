#pragma once

#include <gl\glew.h>
#include <IL\il.h>

class Texture
{
public:
	Texture(const char *source);
	~Texture();

	GLuint texObj() const;

private:
	ILuint m_image;
	GLuint m_texture;
	unsigned int m_width;
	unsigned int m_height;
	unsigned int m_bpp;
};

