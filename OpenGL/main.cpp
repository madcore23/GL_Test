#pragma once 

#include <stdio.h>
#include <Windows.h>
#include <vector>
#include <iostream>

#define GLEW_STATIC
#include <gl\glew.h>

#define GLFW_INCLUDE_GLU
#include <GLFW\glfw3.h>

#include <IL\il.h>
#include <IL\ilu.h>

#pragma comment(lib, "devil.lib")
#pragma comment(lib, "ILU.lib")

#include "Shader.h"
#include "Program.h"
#include "Texture.h"
#include "Mesh.h"
#include "Camera.h"
#include "ControlManager.h"

#include "auxMath.h"

ControlManager g_controlManager;

static void error_callback(int error, const char* description)
{
	fputs(description, stderr);
}
static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);

	if (action == GLFW_PRESS)
		g_controlManager.keyPressNotify(key, scancode, mods);
	else if (action == GLFW_RELEASE)
		g_controlManager.keyReleaseNotify(key, scancode, mods);
}
static void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
	static int xCenter = GetSystemMetrics(SM_CXSCREEN)/2;
	static int yCenter = GetSystemMetrics(SM_CYSCREEN)/2;

	//_RPT2(0, "XPos: %f, YPos: %f\n", xpos-xCenter, ypos-yCenter);

	g_controlManager.mouseMoveNotify(xpos - xCenter, ypos - yCenter);
}
void render(Program program);

GLFWwindow* createGlfwWindow(int width, int height, const char* title, bool fullscreen, int glMajor, int glMinor);

bool auxLibInit();

int main()
{
	GLFWwindow* window = createGlfwWindow(GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN), "Simple example", true, 4, 0);

	if (!window)
	{
		glfwTerminate();
		OutputDebugString("Couldn't create window!\n");
		return -1;
	}

	glfwMakeContextCurrent(window);

	if (!auxLibInit())
	{
		OutputDebugString("Couldn't load auxiliary libraries!");
		return false;
	}

	glfwSwapInterval(0);

	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	glfwSetKeyCallback(window, key_callback);
	glfwSetCursorPosCallback(window, mouse_callback);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);

	Shader vShader(GL_VERTEX_SHADER);
	Shader fShader(GL_FRAGMENT_SHADER);

	if (!vShader.loadShader("vertex_shader.vert"))
	{
		OutputDebugString("Couldn't load vertex shader!\n");
		return -1;
	}

	if (!fShader.loadShader("fragment_shader.frag"))
	{
		OutputDebugString("Couldn't load fragment shader!\n");
		return -1;
	}

	std::vector<Shader> shaders;
	shaders.reserve(2);					// TODO: FIX SHADER CLASS TO HANDLE COPYING PROPERLY

	shaders.push_back(vShader);
	shaders.push_back(fShader);

	Program shaderProgram;
	if (!shaderProgram.linkProgram(shaders))
	{
		OutputDebugString("Couldn't link shader program!\n");
		return -1;
	}

	Texture texture("Cube_tex.png");

	Mesh meshData;
	meshData.loadMesh("test");

	Camera camObject;
	g_controlManager.addListener(&camObject);

	auxMath::Matrix4x4 pMatrix = auxMath::perspectiveProjMatrix(0.01f, 100.0f, 16.0f/9.0f, 90.0f);

	float currentTime = glfwGetTime();
	
	while (!glfwWindowShouldClose(window))
	{
		float newTime = glfwGetTime();
		camObject.update(newTime-currentTime);
		currentTime = newTime;

		float ratio;
		int width, height;

		glfwGetFramebufferSize(window, &width, &height);

		ratio = width / (float)height;

		auxMath::Matrix4x4 mMatrix = auxMath::translationMatrix(auxMath::Vector3(0.0f, 0.0f, -3.0f)) * 
			auxMath::rotationMatrix(glfwGetTime()*10.0f, auxMath::Vector3(0.0f, 1.0f, 0.0f));
			
		auxMath::Matrix4x4 vMatrix = camObject.viewMatrix();

		auxMath::Matrix4x4 mvMatrix = vMatrix * mMatrix;

		auxMath::Matrix4x4 resultMatrix = pMatrix * vMatrix * mMatrix;

		auxMath::Vector3 light(-10.f, 5.f, 10.f);

		glViewport(0, 0, width, height);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		shaderProgram.useProgram();

		int mPos = glGetUniformLocation(shaderProgram.getProgram(), "mMatrix");
		int mvPos = glGetUniformLocation(shaderProgram.getProgram(), "mvMatrix");
		int mvpPos = glGetUniformLocation(shaderProgram.getProgram(), "mvpMatrix");
		int lightPos = glGetUniformLocation(shaderProgram.getProgram(), "v_lightPos");
		int texPos = glGetUniformLocation(shaderProgram.getProgram(), "f_texture");

		glUniformMatrix4fv(mPos, 1, GL_FALSE, mMatrix.data());
		glUniformMatrix4fv(mvPos, 1, GL_FALSE, mvMatrix.data());
		glUniformMatrix4fv(mvpPos, 1, GL_FALSE, resultMatrix.data());
		glUniform3f(lightPos, light.x(), light.y(), light.z());

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texture.texObj());
		glUniform1i(texPos, 0);

		glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0);

		//glDisableVertexAttribArray(0);

		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glfwDestroyWindow(window);

	glfwTerminate();

	return 0;
}

GLFWwindow* createGlfwWindow(int width, int height, const char* title, bool fullscreen, int glMajor, int glMinor)
{
	GLFWwindow* window;

	if (!glfwInit())
	{
		OutputDebugString("Couldn't init GLFW!\n");
		return nullptr;
	}

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, glMajor);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, glMinor);

	//window = glfwCreateWindow(GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN), "Simple example", glfwGetPrimaryMonitor(), NULL);
	window = glfwCreateWindow(width, height, title, (fullscreen) ? glfwGetPrimaryMonitor() : NULL, NULL);

	return window;
}

bool auxLibInit()
{
	glewExperimental = true;

	if (glewInit() != GLEW_OK)
	{
		OutputDebugString("Couldn't init GLEW!\n");
		return false;
	}

	GLenum error = glGetError();

	ilInit();
	iluInit();
}

void render(Program program)
{
	program.useProgram();
}