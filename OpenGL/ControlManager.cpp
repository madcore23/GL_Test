#include "ControlManager.h"


ControlManager::ControlManager()
{
}


ControlManager::~ControlManager()
{
}

void ControlManager::addListener(Entity *p_listener)
{
	m_listeners.push_back(p_listener);
}

void ControlManager::keyPressNotify(int p_key, int p_scancode, int p_mods)
{
	for (auto &listener : m_listeners)
		listener->onKeyPress(p_key, p_scancode, p_mods);
}

void ControlManager::keyReleaseNotify(int p_key, int p_scancode, int p_mods)
{
	for (auto &listener : m_listeners)
		listener->onKeyRelease(p_key, p_scancode, p_mods);
}

void ControlManager::mouseMoveNotify(double xpos, double ypos)
{
	for (auto &listener : m_listeners)
		listener->onMouseMove(xpos, ypos);
}