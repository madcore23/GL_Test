#include "Entity.h"

Entity::Entity() : m_modelMatrix(), m_position(0, 0, 0), m_axis(1.0f, 0, 0), m_rotAngle(0)
{
	for (int i = 0; i < 4; ++i)
		m_modelMatrix(i, i) = 1.0f;
}

Entity::~Entity()
{
}

void Entity::moveTo(Vector3 shift)
{
	m_modelMatrix(3, 0) = shift.x();
	m_modelMatrix(3, 1) = shift.y();
	m_modelMatrix(3, 2) = shift.z();
}

void Entity::moveBy(Vector3 shift)
{
	/*m_modelMatrix(3, 0) += shift.x();
	m_modelMatrix(3, 1) += shift.y();
	m_modelMatrix(3, 2) += shift.z();*/

	m_modelMatrix = auxMath::translationMatrix(shift) * m_modelMatrix;
}

void Entity::rotateTo(float angle, Vector3 axis)
{
	Matrix4x4 rotMatrix = auxMath::rotationMatrix(angle, axis);

	for (int i = 0; i < 3; ++i)
		for (int j = 0; j < 3; ++j)
			m_modelMatrix(i, j) = rotMatrix(i, j);
}

/*void Entity::rotateBy(float p_angle, Vector3 p_axis)
{
	auxMath::Matrix4x4 rotM = auxMath::rotationMatrix(p_angle, p_axis) * auxMath::rotationMatrix(m_rotAngle, m_axis);

	m_rotAngle = acosf((rotM(0, 0) + rotM(1, 1) + rotM(2, 2) - 1.0f) / 2.0f) * 180.0f / auxMath::piConst;

	float matA = rotM(1, 2) - rotM(2, 1);
	float matB = rotM(2, 0) - rotM(0, 2);
	float matC = rotM(0, 1) - rotM(1, 0);

	float sqrSum = sqrtf(matA*matA + matB*matB + matC*matC);

	m_axis = Vector3( matA / sqrSum,
					  matB / sqrSum,
					  matC / sqrSum);
}*/

void Entity::rotateBy(float angle, Vector3 axis)
{
	m_modelMatrix = auxMath::rotationMatrix(angle, axis) * m_modelMatrix;
}