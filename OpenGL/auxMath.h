#pragma once

#include <cmath>

namespace auxMath
{
	static float piConst = 3.141592f;

	class Matrix4x4
	{
	public:
		Matrix4x4();

		float& operator()(int i, int j);
		const float operator()(int i, int j) const;
		const Matrix4x4 operator*(const Matrix4x4& rhs) const;

		const float* data() const { return m_field; }

	private:
		float* m_field;
	};

	class Vector3
	{
	public:
		Vector3() = default;
		Vector3(float p_x, float p_y, float p_z);

		const float x() const { return m_x; }
		const float y() const { return m_y; }
		const float z() const { return m_z; }

		const Vector3 operator*(const Vector3& rhs) const;
		Vector3& operator+=(const Vector3& rhs);
		Vector3 operator-() const;

		void normalize();

	private:
		float m_x;
		float m_y;
		float m_z;
	};

	Matrix4x4 perspectiveProjMatrix(float p_near, float p_far, float p_right, float p_left, float p_top, float p_bottom);
	Matrix4x4 perspectiveProjMatrix(float p_near, float p_far, float p_aspectRatio, float p_FOV);

	Matrix4x4 identityMatrix();
	Matrix4x4 translationMatrix(const Vector3& p_shift);
	Matrix4x4 rotationMatrix(const float p_angle, const Vector3& p_axis);
	Matrix4x4 scalingMatrix(const Vector3& p_scale);
}
