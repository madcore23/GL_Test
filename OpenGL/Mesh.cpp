#include "Mesh.h"

Mesh::Mesh() : m_VAO(0), m_buffer(nullptr), m_bufNum(0)
{
}

Mesh::~Mesh()
{
	if (m_VAO)
	{
		glDeleteVertexArrays(1, &m_VAO);
	}
	if (m_bufNum != 0)
	{
		glDeleteBuffers(m_bufNum, m_buffer);
	}
	delete[] m_buffer;
}

void Mesh::loadMesh(const char* filename)
{
	float vertData[] = {
		-1.0f, -1.0f, 1.0f,		// Front face
		1.0f, -1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, 1.0f,

		1.0f, -1.0f, -1.0f,		// Back face
		-1.0f, -1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,
		1.0f, 1.0f, -1.0f,

		1.0f, -1.0f, 1.0f,		// Right face
		1.0f, -1.0f, -1.0f,
		1.0f, 1.0f, -1.0f,
		1.0f, 1.0f, 1.0f,

		-1.0f, -1.0f, -1.0f,		// Left face
		-1.0f, -1.0f, 1.0f,
		-1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, -1.0f,

		-1.0f, 1.0f, 1.0f,		// Top face
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,

		-1.0f, -1.0f, -1.0f,		// Bottom face
		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f
	};

	float texData[] = {
		0.f, 1.f - 42.f/128.f,
		42.f/128.f, 1.f - 42.f/128.f,
		42.f/128.f, 1.0f,
		0.0f, 1.0f,

		84.f/128.f, 1.f - 84.f/128.f,
		126.f/128.f, 1.f - 84.f/128.f,
		126.f/128.f, 1.f - 42.f/128.f,
		84.f/128.f, 1.f - 42.f/128.f,

		42.f/128.f, 1.f - 42.f/128.f,
		84.f/128.f, 1.f - 42.f/128.f,
		84.f/128.f, 1.0f,
		42.f/128.f, 1.0f,

		42.f/128.f, 1.f - 84.f/128.f,
		84.f/128.f, 1.f - 84.f/128.f,
		84.f/128.f, 1.f - 42.f/128.f,
		42.f/128.f, 1.f - 42.f/128.f,

		0.f, 1.f - 84.f/128.f,
		42.f/128.f, 1.f - 84.f/128.f,
		42.f/128.f, 1.f - 42.f/128.f,
		0.f, 1.f - 42.f/128.f,

		84.f/128.f, 1.f - 42.f/128.f,
		126.f/128.f, 1.f - 42.f/128.f,
		126.f/128.f, 1.f,
		84.f/128.f, 1.f,
	};

	float normalData[] = {
		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,

		0.0f, 0.0f, -1.0f,
		0.0f, 0.0f, -1.0f,
		0.0f, 0.0f, -1.0f,
		0.0f, 0.0f, -1.0f,

		1.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f,

		-1.0f, 0.0f, 0.0f,
		-1.0f, 0.0f, 0.0f,
		-1.0f, 0.0f, 0.0f,
		-1.0f, 0.0f, 0.0f,

		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,

		0.0f, -1.0f, 0.0f,
		0.0f, -1.0f, 0.0f,
		0.0f, -1.0f, 0.0f,
		0.0f, -1.0f, 0.0f,
	};

	unsigned int indexData[] = { 
			0, 1, 2, 2, 3, 0, 
			4, 5, 6, 6, 7, 4,
			8, 9, 10, 10, 11, 8,
			12, 13, 14, 14, 15, 12,
			16, 17, 18, 18, 19, 16,
			20, 21, 22, 22, 23, 20
	};

	glGenVertexArrays(1, &m_VAO);
	glBindVertexArray(m_VAO);

	m_bufNum = 4;
	m_buffer = new GLuint[m_bufNum];

	glGenBuffers(m_bufNum, m_buffer);

	glBindBuffer(GL_ARRAY_BUFFER, m_buffer[0]);
	glBufferData(GL_ARRAY_BUFFER, 24 * 3 * 4, vertData, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, m_buffer[1]);
	glBufferData(GL_ARRAY_BUFFER, 24 * 3 * 4, normalData, GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, m_buffer[2]);
	glBufferData(GL_ARRAY_BUFFER, 24 * 2 * 4, texData, GL_STATIC_DRAW);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_buffer[3]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6 * 6 * 4, indexData, GL_STATIC_DRAW);
}
