#pragma once

#include <gl\glew.h>
#include <vector>

#include "Shader.h"

class Program
{
public:
	Program();
	~Program();

	bool linkProgram(std::vector<Shader> &shaders);
	void useProgram();
	GLuint getProgram() const;
private:
	GLuint programObject;
};

