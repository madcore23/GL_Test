#pragma once

#include <gl\glew.h>

class Shader
{
public:
	Shader(GLenum type);
	~Shader();

	bool loadShader(const char *source);
	GLuint getShader() const;
private:
	GLuint shaderObject;

	int useCount;
};

