#include <fstream>
#include <Windows.h>

#include "Shader.h"

Shader::Shader(GLenum type) : shaderObject(glCreateShader(type)), useCount(0)
{
	++useCount;
}

Shader::~Shader()
{
	--useCount;

	if (useCount == 0)
		glDeleteShader(shaderObject);
}

bool Shader::loadShader(const char *source)
{
	if (shaderObject == 0)
	{
		return false;
	}

	std::ifstream file(source);

	file.seekg(0, std::ios::end);
	int length = file.tellg();
	file.seekg(0, std::ios::beg);

	char *sourceCode = new char[length+1];
	memset(sourceCode, 0, length + 1);
	file.read(sourceCode, length);
	sourceCode[length] = '\0';

	glShaderSource(shaderObject, 1, &sourceCode, NULL);

	if (glGetError() != GL_NO_ERROR)
	{
		int errorLogLength = 0;
		glGetShaderiv(shaderObject, GL_INFO_LOG_LENGTH, &errorLogLength);
		char *errorLog = new char[errorLogLength+1];
		errorLog[errorLogLength] = '\0';
		
		glGetShaderInfoLog(shaderObject, errorLogLength, NULL, errorLog);

		OutputDebugString(errorLog);

		delete[] errorLog;

		return false;
	}

	glCompileShader(shaderObject);

	int status = 0;
	glGetShaderiv(shaderObject, GL_COMPILE_STATUS, &status);

	if (status != true)
	{
		int errorLogLength = 0;
		glGetShaderiv(shaderObject, GL_INFO_LOG_LENGTH, &errorLogLength);
		char *errorLog = new char[errorLogLength + 1];
		errorLog[errorLogLength] = '\0';

		glGetShaderInfoLog(shaderObject, errorLogLength, NULL, errorLog);

		OutputDebugString(errorLog);

		delete[] errorLog;

		return false;
	}

	GLboolean isShader = glIsShader(shaderObject);

	return true;
}

GLuint Shader::getShader() const
{
	return shaderObject;
}
