#version 400

uniform sampler2D f_texture;

in vec3 f_viewVec;
in vec3 f_normal;
in vec3 f_lightPos;
in vec2 f_texCoord;

out vec4 color;

vec3 phongReflection(vec3 light, vec3 normal, vec3 viewVec)		// TODO: CHECK MATH
{
	vec3 diffColor = texture2D(f_texture, f_texCoord).xyz;
	vec3 specColor = vec3(1.0, 1.0, 1.0);

	float shininess = 100.0;

	vec3 ambIntensity = vec3(0.1, 0.1, 0.1);

	float diff = max(0.0, dot(light, normal));
	vec3 diffIntensity = vec3(diff, diff, diff);

	float spec = pow( max(0.0, dot( normalize(reflect(-light, normal)), viewVec)), shininess);
	vec3 specIntensity = vec3(spec, spec, spec);

	vec3 resultColor = diffColor * min(vec3(1.0, 1.0, 1.0), ambIntensity + diffIntensity) + specColor*specIntensity;
	resultColor = min(vec3(1.0, 1.0, 1.0), resultColor);

	return resultColor;
}

void main()
{
	color = vec4(phongReflection(normalize(f_lightPos), normalize(f_normal), normalize(f_viewVec)), 1.0f);

	//color = 0.3*vec4(f_normal, 0.0f) + texture2D(f_texture, f_texCoord);
}