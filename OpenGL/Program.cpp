#include "Program.h"

#include <Windows.h>

Program::Program() : programObject(glCreateProgram())
{
}

Program::~Program()
{
	glDeleteProgram(programObject);
}

bool Program::linkProgram(std::vector<Shader> &shaders)
{
	if (programObject == 0)
	{
		OutputDebugString("Null program object!");
		return false;
	}

	for (const auto &shader : shaders)
	{
		//GLboolean isShader = glIsShader(shader.getShader());
		//GLboolean isProgram = glIsProgram(programObject);

		glAttachShader(programObject, shader.getShader());

		if (glGetError() != GL_NO_ERROR)
		{
			int infoLogLength = 0;
			glGetProgramiv(programObject, GL_INFO_LOG_LENGTH, &infoLogLength);

			char *infoLog = new char[infoLogLength+1];
			glGetProgramInfoLog(programObject, infoLogLength, NULL, infoLog);
			infoLog[infoLogLength] = '\0';

			OutputDebugString(infoLog);

			return false;
		}
	}

	glLinkProgram(programObject);

	for (const auto &shader : shaders)
	{
		glDetachShader(programObject, shader.getShader());

		if (glGetError() != GL_NO_ERROR)
		{
			int infoLogLength = 0;
			glGetProgramiv(programObject, GL_INFO_LOG_LENGTH, &infoLogLength);

			char *infoLog = new char[infoLogLength + 1];
			glGetProgramInfoLog(programObject, infoLogLength, NULL, infoLog);
			infoLog[infoLogLength] = '\0';

			OutputDebugString(infoLog);

			return false;
		}
	}

	int status = 0;
	glGetProgramiv(programObject, GL_LINK_STATUS, &status);

	if (status != true)
	{
		int infoLogLength = 0;
		glGetProgramiv(programObject, GL_INFO_LOG_LENGTH, &infoLogLength);

		char *infoLog = new char[infoLogLength + 1];
		glGetProgramInfoLog(programObject, infoLogLength, NULL, infoLog);
		infoLog[infoLogLength] = '\0';

		OutputDebugString(infoLog);

		return false;
	}

	return true;
}

void Program::useProgram()
{
	glUseProgram(programObject);
}

GLuint Program::getProgram() const
{
	return programObject;
}